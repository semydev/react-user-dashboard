import React from 'react'
import { Redirect, Route } from "react-router-dom"
import { isExpired, decodeToken } from "react-jwt";

export const AuthenticatedRoute = ({ component, path, exact}) => {
	const token = localStorage.getItem("authorization")

	if (token) {
		// Check the authorization can be parsed...
		try {
			const myDecodedToken = decodeToken(token);
			const isMyTokenExpired = isExpired(token);

			console.log(`AuthenticatedRoute: ${path}`)
			if (isMyTokenExpired) {
				localStorage.removeItem("authorization")
				return <Redirect to="/login" />
			} else if (myDecodedToken) {
				return <Route component={component} path={path} exact={exact} />
			}
		} catch (err) {
			console.log(`[ERROR] Error parsing JWT access token - ${err.message}`)

			// Something went wrong, clear their token and make them login.
			localStorage.removeItem("authorization")
			return <Redirect to="/login" />
		}
	}

	console.log(`AuthenticatedRoute: ${path} - Redirecting`)
	return <Redirect to={`/login?path=${path}`} />
}

export const AdminAuthenticatedRoute = ({ component, path, exact}) => {
	const token = localStorage.getItem("authorization")

	if (token) {
		// Check the authorization can be parsed...
		try {
			const myDecodedToken = decodeToken(token);
			const isMyTokenExpired = isExpired(token);

			console.log(`AdminAuthenticatedRoute: ${path}`)
			if (isMyTokenExpired) {
				localStorage.removeItem("authorization")
				return <Redirect to="/login" />
			} else if (myDecodedToken) {
				// if (myDecodedToken.user && myDecodedToken.user.role === "Admin") {
				// 	return <Route component={component} path={path} exact={exact} />
				// }
				return <Route component={component} path={path} exact={exact} />
			}
		} catch (err) {
			console.log(`[ERROR] Error parsing JWT access token - ${err.message}`)

			// Something went wrong, clear their token and make them login.
			localStorage.removeItem("authorization")
			return <Redirect to="/login" />
		}
	}

	console.log(`AdminAuthenticatedRoute: ${path} - Redirecting`)
	return <Redirect to={`/login?path=${path}`} />
}

export const UnauthenticatedRoute = ({ component, path, exact }) => {
    if (localStorage.getItem("authorization")) {
        return <Redirect to="/dashboard" />
    }

    return <Route component={component} path={path} exact={exact} />
}
