import { BrowserRouter, Route, Switch } from "react-router-dom"

import "./assets/scss/App.scss"

import ScrollToTop from './components/ScrollToTop';
import HomePage from './components/HomePage';
import Dashboard from './components/Dashboard';
import Auth from './components/Auth/Auth';

import AdminHome from './components/admin/AdminHome';
import AdminTeam from './components/admin/AdminTeam';
import AdminUsers from './components/admin/users/AdminUsers';
import AdminUsersEdit from './components/admin/users/AdminUsersEdit';
import AdminUsersView from './components/admin/users/AdminUsersView';
import AdminPlans from './components/admin/plans/AdminPlans';
import AdminPlansView from './components/admin/plans/AdminPlansView';

import { AdminAuthenticatedRoute, AuthenticatedRoute, UnauthenticatedRoute } from "./helpers/Authenticated"
import { UserContextProvider } from './context/UserContext';

import FetchRetry from "fetch-retry"

// Redefine the fetch function to use as an interceptor
var originalFetch = window.fetch
window.fetch = function () {
	if (arguments[1].headers && arguments[1].headers["Authorization"]) {
		arguments[1].headers["Authorization"] = `Bearer ${localStorage.getItem("authorization")}`
	}

	return originalFetch.apply(this, arguments).then(async function (res) {
		// Check for expired token and create a new token
		for (const [name, value] of res.headers.entries()) {
			if (name.toLowerCase() === "content-type" && value.toLowerCase().includes("application/json")) {
				// Parse json automatically into request
				res.json = await res.json()
			}
		}

		if (res.status === 401) {
			// Check if the 401 is caused by an invalid access token
			if (res.json && res.json.message === "invalid_token") {
				// Attempt to refresh the user's access token.
				await fetch(`${process.env.REACT_APP_HOST}/api/auth/token`, {
					method: "POST",
					credentials: "include",
					headers: {
						"Content-type": "application/json",
						"Accept": "application/json",
					},
				}).then(res => {
					if (res.status === 200) {
						// Successfully refreshed token,
						localStorage.setItem("authorization", res.json.access_token)
					} else {
						// There was an error trying to refresh the token
						// Log the user out
						localStorage.removeItem("authorization")
					}
					return
				})
			}
		}

		return res
	})
}

window.fetch = FetchRetry(window.fetch, {
	retryOn: function (attempt, error, response) {
		// retry on any network error, or 4xx or 5xx status codes
		if (attempt > 2) {
			return false
		}

		if (error !== null || (response.status === 401 && response.json.message === "invalid_token")) {
			return true
		}
	},
})

function App() {
  return (
    <div className="App">
      <BrowserRouter>
	  	<UserContextProvider>
			<ScrollToTop />
			<Switch>
			<Route exact path="/" component={HomePage} />
			<UnauthenticatedRoute exact path="/login" component={() => <Auth type="Login" />} />
			<UnauthenticatedRoute exact path="/register" component={() => <Auth type="Register" />} />
			<UnauthenticatedRoute exact path="/forgot" component={() => <Auth type="Forgot" />} />

			<AuthenticatedRoute exact path="/dashboard" component={Dashboard} />

			<AdminAuthenticatedRoute exact path="/admin" component={AdminHome} />
			<AdminAuthenticatedRoute exact path="/admin/users" component={AdminUsers} />
			<AdminAuthenticatedRoute exact path="/admin/users/:id" component={AdminUsersView} />
			<AdminAuthenticatedRoute exact path="/admin/users/:id" component={AdminUsersEdit} />
			<AdminAuthenticatedRoute exact path="/admin/plans" component={AdminPlans} />
			<AdminAuthenticatedRoute exact path="/admin/plans/:id" component={AdminPlansView} />
			<AdminAuthenticatedRoute exact path="/admin/team" component={AdminTeam} />


			<AdminAuthenticatedRoute exact path="/admin" component={AdminHome} />
			<AdminAuthenticatedRoute exact path="/admin/users" component={AdminUsers} />
			<AdminAuthenticatedRoute exact path="/admin/users/:id" component={AdminUsersView} />
			<AdminAuthenticatedRoute exact path="/admin/users/:id/edit" component={AdminUsersEdit} />

			<UnauthenticatedRoute component={() => <Auth type="Resp_404" />} />
			</Switch>
		</UserContextProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
