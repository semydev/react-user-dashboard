const UnbindLicenseModal = ({ modalIsOpen, setModalIsOpen }) => {
    const showHideClassName = modalIsOpen ? "modal-dialog modal-dialog-centered display-block" : "modal display-none";

    return (
        <div className={showHideClassName} id="modal">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 id="modal-basic-title" className="modal-title">Are you sure?</h4>
                    <button type="button" aria-label="Close" className="close" onClick={() => setModalIsOpen(!modalIsOpen) }>
                        <span aria-hidden="true">✖</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div> This action is irreversible. </div>
                    <div>  If you want to unbind your license press confirm. </div>
                </div>
                <div className="modal-footer align-items-center justify-content-center">
                    <button
                        className={`btn btn-custom btn-lg shadow-none save-button}`}
                        type="submit"
                    >
                        Confirm    
                    </button>
                </div>
            </div>
        </div>
    )
}

export default UnbindLicenseModal
