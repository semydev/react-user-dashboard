const UpdateEmail = ({ modalIsOpen, setModalIsOpen }) => {
    const showHideClassName = modalIsOpen ? "modal-dialog modal-dialog-centered display-block" : "modal display-none";

    return (
        <div className={showHideClassName} id="modal">
            <div className="modal-content">
                <div className="modal-header">
                    <h4 id="modal-basic-title" className="modal-title">Update Email</h4>
                    <button type="button" aria-label="Close" className="close" onClick={() => setModalIsOpen(!modalIsOpen) }>
                        <span aria-hidden="true">✖</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div role="alert" className="alert alert-danger"></div>
                    <input type="email" 
                        required 
                        id="userEmail" 
                        aria-describedby="emailHelp" 
                        placeholder="Email" 
                        className="form-control shadow-none ng-untouched ng-pristine ng-valid"
                        pattern="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}$"
                    ></input>
                </div>
                <div className="modal-footer align-items-center justify-content-center">
                    <button
                        className={`btn btn-custom btn-lg shadow-none save-button}`}
                        type="submit"
                    >
                        Update    
                    </button>
                </div>
            </div>
        </div>
    )
}

export default UpdateEmail
