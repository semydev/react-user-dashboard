import React from 'react'

export const HomePage = () => {
    return (
        <div>
            <div className="context">
                <h1>The Sole Service Dashboard</h1>
                <p>Under Construction...</p>
            </div>

            <div className="area">
                <ul className="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    )
}

export default HomePage;