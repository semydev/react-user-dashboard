import React, { useState, useEffect } from "react";

import logo from "../../assets/img/tss-logo.svg"

import Login from "./Login"
import Register from "./Register"
import Forgot from "./Forgot"
import Resp_404 from "./Resp_404"

const components = {
	Register,
	Login,
	Forgot,
	Resp_404,
}

const Auth = ({ type }) => {
    const [success, setSuccess] = useState(null);
    const [error, setError] = useState(null);

    const getComponent = () => {
		const Component = components[type]

		return (
			<Component
				handleError={(msg = false) => handleError(msg)}
				handleSuccess={(msg = false) => handleSuccess(msg)}
				error={error}
				success={success}
			/>
		)
    }

    const handleError = (msg) => {
        setError(msg);
        setSuccess(false);
	}

	const handleSuccess = (msg) => {
        setError(false);
        setSuccess(msg);
    }

    useEffect(()=> {
		if (error) {
			setTimeout(()=> {
				setError(null);
			}, 10000);
		}
	}, [error]);
		
    return (
        <div className="area">
            <div className="auth-wrapper">
                <div className="auth-container">
                    <header className="brand">
                        <img src={logo} alt="The Sole Service Logo" />
                        <span>The Sole Service</span>
                    </header>

                    {getComponent()}
                </div>
            </div>

            <ul className="circles">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    )
}

export default Auth;
