import { useState, useEffect } from 'react'
import { NavLink } from "react-router-dom"

const Register = ({ handleError, handleSuccess, error, success }) => {
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [terms, setTerms] = useState('');
    const [loading, setLoading] = useState(false);

    const handleSubmit = (e) => {
		e.preventDefault()

		setLoading(true);

		fetch(`${process.env.REACT_APP_HOST}/api/account/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",
			},
			body: JSON.stringify({
				email: email,
				password: password,
				confirm_password: confirmPassword,
				terms: terms,
			}),
		}).then(async res => {
            setLoading(false)

			const data = res.json

			if (res.status === 200) {
				const path = new URLSearchParams(window.location.search).get("path") || "/dashboard";

				// Set authorization token
				localStorage.setItem("authorization", data.access_token)
				window.location.href = path;
			} else {
				handleError(data.message)
			}
        });
    }

    useEffect(()=> {
		if (error) {
			setTimeout(()=> {
				handleError(null);
			}, 10000);
		}
    }, [handleError, error]);
    
    return (
        <form onSubmit={handleSubmit}>
			<h2>You must be new</h2>
			<p>Join an established monitor group dedicated to helping its members cop limited release items</p>
			<input type="text" onChange={(e)=>setEmail(e.target.value)} placeholder="Email address" name="email" />
			<input type="password" onChange={(e)=>setPassword(e.target.value)} placeholder="Password" name="password" />
			<input type="password" onChange={(e)=>setConfirmPassword(e.target.value)} placeholder="Confirm Password" name="confirmPassword" />
			<label>
				<input type="checkbox" onChange={(e)=>setTerms(e.target.value)} name="terms" />
				<span>
					I accept the <NavLink to="/terms">terms and conditions</NavLink>
				</span>
			</label>
			{error && <span className="error">{error}</span>}
			<div className="buttons">
				<button type="submit" className="btn">
					<span>Create Account</span>
					{loading ? <i className="spin fal fa-spinner-third"></i> : <i className="icon-icon-arrow-right-long"></i>}
				</button>
				<NavLink to="/login">Have an account?</NavLink>
			</div>
		</form>
    )
}

export default Register
