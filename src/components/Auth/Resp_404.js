import React from 'react'

import { NavLink } from 'react-router-dom'

const Resp_404 = () => {
    return (
        <form>
			<h1>Whoops,</h1>
			<p>Sorry, we can't find that page! Don't worry though, click the button below.</p>
			<NavLink className="btn btn-submit" to="/">
				Back
			</NavLink>
		</form>
    )
}

export default Resp_404
