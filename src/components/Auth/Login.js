import { useState, useEffect, useContext } from 'react'
import { NavLink } from "react-router-dom"

import { SET_USER, UserContext } from '../../context/UserContext';

const Login = ({ handleError, handleSuccess, error, success }) => {
	const [user, userDispatch] = useContext(UserContext);
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    
    const handleSubmit = async (e) => {
		e.preventDefault()

		setLoading(false);

		return fetch(`${process.env.REACT_APP_HOST}/api/account/login`, {
			method: "POST",
			credentials: "include",
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json",
			},
			body: JSON.stringify({ email, password }),
		}).then((response) => {
			const data = response.json;
			if (response.status === 200) {
				const path = new URLSearchParams(window.location.search).get("path") || "/dashboard";
				localStorage.setItem("authorization", data.access_token)

				userDispatch({
					type: SET_USER,
					payload: { value: { isLoggedIn: true, email } },
				});

				window.location.href = path;
			} else {
				console.log(error.message)
				handleError(data.message)
			}
		})
	}

    useEffect(()=> {
		if (error) {
			setTimeout(()=> {
				handleError(null);
			}, 10000);
		}
    }, [handleError, error]);
    
    return (
        <form onSubmit={handleSubmit}>
            <h2>Hello again,</h2>
            <p>Enter your email and password to login.</p>
            {error && <span className="error">{error}</span>}
            <input type="text" value={email} onChange={(e) => setEmail(e.target.value)} placeholder="Email Address" name="email" />
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Password" name="password" />
            <NavLink to="/forgot" className="auth-notification">
				Forgot password?
			</NavLink>
			<div className="buttons">
				<button type="submit" className="btn">
					<span>Login</span>
					{loading ? <i className="spin fal fa-spinner-third"></i> : <i className="icon-icon-arrow-right-long"></i>}
				</button>
				<NavLink to="/register">Create account</NavLink>
			</div>
        </form>
    )
}

export default Login;
