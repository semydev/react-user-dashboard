import { useState, useEffect } from 'react'
import { NavLink } from "react-router-dom"

const Forgot = ({ handleError, handleSuccess, error, success }) => {
    const [email, setEmail] = useState('');
    
    const handleSubmit = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_HOST}/api/account/password-reset`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
			},
			body: JSON.stringify({
				email: email,
			}),
		}).then(async res => {
			const data = res.json
			if (res.status === 200) {
				handleSuccess(data.message)
			} else {
				handleError(data.message)
			}
		})
    }

    useEffect(()=> {
		if (error) {
			setTimeout(()=> {
				handleError(null);
			}, 10000);
		}
    }, [handleError, error]);
    
    return (
        <form onSubmit={handleSubmit}>
			<h2>Forgot password?</h2>
			<p>You will be sent an email with instructions on how to change your password.</p>
			<input type="text" onChange={(e) => setEmail(e.target.value)} placeholder="Email address" name="email" />
			{error && <span className="error">{error}</span>}
			{success && <span className="success">{success}</span>}
			<div className="buttons">
				<button type="submit" className="btn">
					<span>Send Email</span>
					<i className="icon-icon-arrow-right-long"></i>
				</button>
				<NavLink to="/login">Login</NavLink>
			</div>
		</form>
    )
}

export default Forgot
