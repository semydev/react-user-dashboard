import React, { useState } from "react";
import { NavLink } from "react-router-dom"
import * as moment from 'moment';
import logo from "../../../assets/img/tss-logo.svg"

const AdminUsers = () => {
	const [users, setUsers] = useState([{
        membership: moment(),
        email: "test@gmail.com",
        discord_id: "Cookie#1738",
        avatar: null,
        username: 'Cookie',
        discriminator: '1738'
    }])

    const handleSubmit = e => {
		e.preventDefault()

		// Find user
		fetch(`${process.env.REACT_APP_HOST}/api/admin/users/search`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				"Accept": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("authorization")}`,
			},
			body: JSON.stringify({
				query: users.id,
			}),
		}).then(async res => {
			const data = res.json
			console.log(data)
			if (res.status === 200) {
				setUsers(data.users)
			}
		})
	}

    const getUsers = () => {
		if (!users) {
			return <p>Search to see results.</p>
		}

		if (users && users.length === 0) {
			return <p>No search results.</p>
		}

        console.log(users)

		const usersMap = users.map((user, i) => {
			const memberStatus = user.membership === null ? null : moment(user.membership).isAfter(moment()) ? "isMember" : "wasMember"
            console.log(user)
			return (
				<NavLink key={i} className="user" to={`/admin/users/${user.id}`}>
					<h3>
						{memberStatus && <i className={`fas fa-circle ${memberStatus}`}></i>}
						{user.email}
					</h3>
					{user.discord_id ? (
						<div className="discord">
							<img
								src={`https://cdn.discordapp.com/avatars/${user.discord_id}/${user.avatar}.webp?size=256`}
								alt="Discord Profile"
								onError={e => (e.target.src = 'https://res.cloudinary.com/dklrin11o/image/twitter_name/w_600/shreyauth.jpg')}
							/>
							<div className="discord-user">
								<h4>{`${user.username}#${user.discriminator}`}</h4>
								<p>{user.discord_id}</p>
							</div>
						</div>
					) : (
						<p>No Slack account linked.</p>
					)}
				</NavLink>
			)
		})

		return users
	}


    return (
        <div>
            <div className="area">
                <div className="dashboard-wrapper">
                    <header>
                        <img src={logo} alt="The Sole Service Logo" />
                    </header>
                </div>

                <div className="text-white admin-wrapper">
                    <div className="container">
                        <form onSubmit={e => handleSubmit(e)}>
                            <h2>Find a user</h2>
                            <div className="input-wrapper">
                                <input type="text" placeholder="Search anything to find a user" />
                                <button className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none">Search</button>
                                <button className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none">Reset Search</button>
                            </div>
                        </form>

                        <div className="view">
                            <h3>Results</h3>
                            <div className="users">
                                <NavLink key="1" className="user" to={`/admin/users/1`}>
                                    <h4>
                                        <i className={`fas fa-circle isMember`}></i>
                                        {users[0].email}
                                    </h4>
                                    <span class="float-right">Joined about 7 days ago</span>
                                    {users[0].discord_id ? (
                                        <div className="discord">
                                            <img
                                                src={`https://cdn.discordapp.com/avatars/${users[0].discord_id}/${users[0].avatar}.webp?size=256`}
                                                alt="Slack Profile"
                                                onError={e => (e.target.src = 'https://res.cloudinary.com/dklrin11o/image/twitter_name/w_600/shreyauth.jpg')}
                                            />
                                            <div className="discord-user">
                                                <h4>{`${users[0].username}#${users[0].discriminator}`}</h4>
                                                <p>{users[0].discord_id}</p>
                                            </div>
                                        </div>
                                    ) : (
                                        <p>No Slack account linked.</p>
                                    )}
                                </NavLink>    
                            </div>
                        </div>
                    </div>
                </div>
                
                <ul className="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>

                <div className="footer footer-light bg-transparent justify-content-between">
                    <button className="btn btn-custom btn-lg shadow-none btn-logout">
                        Logout
                    </button >
                </div>
            </div>
        </div>
    )
}

export default AdminUsers
