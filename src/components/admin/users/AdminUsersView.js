import React, { useState } from "react";
import { NavLink } from "react-router-dom"
import logo from "../../../assets/img/tss-logo.svg"

const AdminUsersView = () => {
    const [user, setUser] = useState({});

    const handleSubmit = e => {
		e.preventDefault()

		// Find user
		fetch(`${process.env.REACT_APP_HOST}/api/admin/users/search`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				"Accept": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("authorization")}`,
			},
			body: JSON.stringify({
				query: user.id,
			}),
		}).then(async res => {
			const data = res.json
			console.log(data)
			if (res.status === 200) {
				setUser(data.user)
			}
		})
	}

    return (
        <div>
            <div className="area">
                <div className="dashboard-wrapper">
                    <header>
                        <img src={logo} alt="The Sole Service Logo" />
                    </header>
                </div>
                <div className="container">
                    <div className="card my-4">
                        <div className="card-body dashcard-header">
                        <img width="85" height="85" alt="" className="rounded-circle float-left mr-3" src="https://cdn.discordapp.com/embed/avatars/3.png"/>
                            <div className="align-vertically">
                                <h4 className="display-6 text-left pt-1">Welcome back,</h4>
                                <h4 className="display-6 text-left pt-1">
                                    Cookie 
                                </h4>
                                <p className="card-text text-left">
                                    <small>Admin since</small>
                                </p>
                            </div>             
                        </div>
                    </div>

                    <div className="row mt-4">
                        <div className="card-deck">
                            <div className="card">
                                <div className="card-body">
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Joined</small>
                                                <div className="mb-4">July 21st, 2021</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Customer ID</small>
                                                <div className="mb-4">cus_JtFVwS8VFk2ov6</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Subscription ID</small>
                                                <div className="mb-4">sub_JtFXgnMeLGpipV</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Next Renewal Date</small>
                                                <div className="mb-4">122/20</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">In Slack</small>
                                                <div className="mb-4">Yes</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Admin Role</small>
                                                <div className="mb-4">No</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="container-fluid overflow-hidden">
                                        <div className="row">
                                            <div className="text-left">
                                                <small className="field-name">Billing Email</small>
                                                <div type="button" className="hoverable">
                                                    nickk.lad@gmail.com
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="card card-body border-0 shadow-low">
                                <h6 className="font-weight-normal">
                                    Comments
                                </h6>
                                <br/>

                                <h6 className="text-center">
                                    <small className="text-muted">
                                        Comments displays comments about a user
                                    </small>
                                </h6>
                            </div>
                        </div>
                    </div>
                
                    <div className="row">
                        <div className="card-deck">
                            <div className="card my-4">
                                <h4>Payments</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <ul className="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>

                <div className="footer footer-light bg-transparent justify-content-between">
                    <button className="btn btn-custom btn-lg shadow-none btn-logout">
                        Logout
                    </button >
                </div>
            </div>
        </div>
    )
}

export default AdminUsersView
