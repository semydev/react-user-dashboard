import React from 'react'
import { NavLink } from "react-router-dom"
import logo from "../../assets/img/tss-logo.svg"

const AdminHome = () => {
    return (
        <div>
            <div className="area">
                <div className="dashboard-wrapper">
                    <header>
                        <img src={logo} alt="The Sole Service Logo" />
                    </header>
                </div>
                <div className="cover d-flex align-items-center text-white">
                    <div className="container">
                        <div className="card my-4">
                            <div className="card-body dashcard-header">
                            <img width="85" height="85" alt="" className="rounded-circle float-left mr-3" src="https://cdn.discordapp.com/embed/avatars/3.png"/>
                                <div className="align-vertically">
                                    <h4 className="display-6 text-left pt-1">Welcome back,</h4>
                                    <h4 className="display-6 text-left pt-1">
                                        Cookie 
                                    </h4>
                                    <p className="card-text text-left">
                                        <small>Admin since</small>
                                    </p>
                                </div>             
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-8 d-flex align-items-stretch">
                                <div className="card card-body border-0 shadow-low">
                                    <h6 className="font-weight-normal">
                                        <i className="fas fa-unlock-alt mr-1"></i> Site password
                                    </h6>

                                    <br/>

                                    <form action="/admin/password" method="POST">
                                        <div className="input-group mb-3">
                                            <input value="" name="password" type="text" className="col-md-8" placeholder="Password" autocomplete="off"/>
                                            <div className="col-md-4">
                                                <button className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none" type="submit">
                                                    Set password
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div className="col-md-4 d-flex align-items-stretch">
                                <div className="card card-body border-0 shadow-low">
                                    <h6 className="font-weight-normal">
                                        Activated users
                                        <small className="float-right">
                                            <a href="/admin/users?activated=yes">
                                                View all
                                            </a>
                                        </small>
                                    </h6>
                                    <br/>
                                    <h1 className="font-weight-normal">
                                        102
                                    </h1>
                                </div>
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col-md-4 mt-4 mb-4 mb-md-0 d-flex align-items-stretch">
                                <div className="card card-body border-0 shadow-low">
                                    <h6 className="font-weight-normal">
                                        New keys this month
                                        <small className="float-right text-muted">
                                            Not including FNF keys
                                        </small>
                                    </h6>

                                    <br/>

                                    <h1 className="font-weight-normal d-inline">
                                        66
                                    </h1>

                                    <h6 className="text-danger font-weight-normal d-inline">
                                        4.38% since last month
                                    </h6>

                                    <hr/>

                                    <h6 className="font-weight-normal">
                                        FNF keys
                                        <small className="float-right text-muted">
                                            Including unbound &amp; expired keys
                                        </small>
                                    </h6>

                                    <br/>

                                    <h1 className="font-weight-normal d-inline">
                                        300
                                    </h1>

                                    <hr/>

                                    <h6 className="font-weight-normal">
                                        Total keys
                                        <small className="float-right text-muted">
                                            Including unbound &amp; expired keys
                                        </small>
                                    </h6>

                                    <br/>
                                    <h1 className="font-weight-normal d-inline">
                                        229
                                    </h1>

                                </div>
                                <br/>
                            </div>

                            <div className="col-md-8 mt-4 mb-4 mb-md-0 d-flex align-items-stretch">
                                <div className="card card-body border-0 shadow-low">
                                    <h6 className="font-weight-normal">
                                        Growth (keys)

                                        <small className="ml-2">
                                            <a href="/admin/charts/display?scope=year" data-remote="true">
                                            &nbsp;Year •&nbsp;
                                            </a>
                                            <a href="/admin/charts/display?scope=month" data-remote="true">
                                            &nbsp;Month •&nbsp;
                                            </a>
                                            <a href="/admin/charts/display?scope=week" data-remote="true">
                                            &nbsp;Week •&nbsp;
                                            </a>
                                            <a href="/admin/charts/display?scope=day" data-remote="true">
                                            &nbsp;Day&nbsp;
                                            </a>
                                        </small>
                                    </h6>
                                    <br/>

                                    <h6 className="text-center">
                                        <small className="text-muted">
                                            Chart displays the amount of keys created every
                                            <span id="scope_display">
                                                month
                                            </span>
                                        </small>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <ul className="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>

                <div className="footer footer-light bg-transparent justify-content-between">
                    <button className="btn btn-custom btn-lg shadow-none btn-logout">
                        Logout
                    </button >
                </div>
            </div>
        </div>
    )
}

export default AdminHome
