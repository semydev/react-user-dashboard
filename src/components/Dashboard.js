import React, { useState, useEffect, useContext } from "react";
import SlackLogin from 'react-slack-login';
import * as moment from 'moment';

import { initialState, SET_USER, UserContext } from '../context/UserContext';

import logo from "../assets/img/tss-logo.svg"
import UpdateCardModal from "./UpdateCardModal";
import UpdateEmailModal from "./UpdateEmailModal";
import UnbindLicenseModal from "./UnbindLicenseModal";

const Dashboard = () => {
    const [user, userDispatch] = useContext(UserContext);
    const [cardModalIsOpen, setCardModalIsOpen] = useState(false);
    const [emailModalIsOpen, setEmailModalIsOpen] = useState(false);
    const [unbindModalIsOpen, setUnbindModalIsOpen] = useState(false);

    useEffect(() => {
        return fetch(`${process.env.REACT_APP_HOST}/api/subscription/me`, {
			headers: {
				'Content-type': 'application/json',
                'Accept': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('authorization')}`,
			},
		}).then((response) => {
            if (response.status === 200) {
                const data = response.json
                if (data.subscription) {
                    data.subscription.subscribed_since = moment(data.subscription.subscribed_since).format('MMM DD, YYYY');
                    data.subscription.expiration = moment(data.subscription.expiration).format('MMM DD, YYYY');
                    userDispatch({
                        type: SET_USER,
                        payload: { value: { ...data, binded: data.binded } },
                    });
                };
            }
		})
    }, [userDispatch]);

    const handleError = (msg) => {
        console.log(msg)
	}

	const handleSuccess = (msg) => {
        console.log(msg)
    }

    const handleLogOut = () => {
        const path = new URLSearchParams(window.location.search).get("path") || "/login";
        localStorage.setItem("authorization", '');
        localStorage.clear();
        userDispatch({
            type: SET_USER,
            payload: {
              value: initialState,
            },
        });
        window.location.href = path;
      }

    return (
        <div>
            <div className="area">
                <div className="dashboard-wrapper">
                    <header>
                        <img src={logo} alt="The Sole Service Logo" />
                    </header>

                    <div className={`dashboard ${
                        !cardModalIsOpen 
                        ? !emailModalIsOpen 
                        ? !unbindModalIsOpen
                        ? "none" 
                        : "blur"
                        : "blur"
                        : "blur"
                    }`}>
                        {!user.binded && (
                            <form>
                                <div className="bind-wrapper">
                                    <h2>Bind Slack Account</h2>
                                    <SlackLogin
                                        redirectUrl='https://localhost:8080/api/slack/redirect'
                                        onFailure={handleError}
                                        onSuccess={handleSuccess}
                                        slackClientId='907075034615.2212824743301'
                                        slackUserScope='identity.basic,identity.email,identity.team,identity.avatar'
                                    />      
                                </div>
                            </form>
                        )}
                        
                        {user.binded && user.subscription.active_subscription && (
                            <div className="cover d-flex align-items-center text-white">
                                <div className="container">
                                    <div className="card my-4">
                                        <div className="card-body dashcard-header">
                                        <img width="85" height="85" alt="" className="rounded-circle float-left mr-3" src="https://cdn.discordapp.com/embed/avatars/3.png"/>
                                            <div className="align-vertically">
                                                <h4 className="display-6 text-left pt-1">Welcome back,</h4>
                                                <h4 className="display-6 text-left pt-1">
                                                    Cookie 
                                                </h4>
                                                <p className="card-text text-left">
                                                    <small>User since {user.subscription.subscribed_since}</small>
                                                </p>
                                            </div>             
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div className="card-deck">
                                            <div className="card">
                                                <div className="card-body">
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row">
                                                            <div className="text-left">
                                                                <small className="field-name">Plan Type</small>
                                                                <div className="mb-4">Renewal</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row">
                                                            <div className="text-left">
                                                                <small className="field-name">Pricing</small>
                                                                <div className="mb-4"> {user.subscription.price} / month</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row">
                                                            <div className="text-left">
                                                                <small className="field-name">Next Renewal Date</small>
                                                                <div className="mb-4">{user.subscription.expiration}</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row">
                                                            <div className="text-left">
                                                                <small className="field-name">Billing Email</small>
                                                                <div type="button" className="hoverable" onClick={() => setEmailModalIsOpen(true)}>
                                                                    {user.subscription.billing_email}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="card card-3">
                                                <div className="card-body">
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row">
                                                            <div className="text-left">
                                                                <small className="field-name">License Key</small>
                                                                <div type="button" className="hoverable">
                                                                    •••••• •••••• •••••• •••••• 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-center">
                                                            <button type="button" className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none mt-2 mb-3">
                                                                Rejoin Slack
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-center">
                                                            <button type="button" className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none mt-2 mb-3" onClick={() => setUnbindModalIsOpen(true)}>
                                                                Unbind License
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="card">
                                                <div className="card-body">
                                                    <div className="cc">
                                                        <img src="https://demo.tldashboards.com/visa.png" alt="Visa logo" height="42" className="cc-issuer" />
                                                        <div className="cc-nr">
                                                            <bold-me>•••• •••• •••• </bold-me>
                                                            {user.subscription.card_details.last_4}
                                                        </div>
                                                        <div className="customer-name">
                                                            <small>{user.subscription.card_details.billing_name}</small>
                                                        </div>
                                                        <div className="expiry">
                                                            <small>{`${user.subscription.card_details.exp_month}/${user.subscription.card_details.exp_year}`}</small>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-center">
                                                            <button 
                                                                type="button" 
                                                                className="btn btn-custom btn-in-dashboard1 btn-lg shadow-none mt-2 mb-3"
                                                                onClick={() => setCardModalIsOpen(true)}
                                                            >
                                                                Update Billing Info
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-center">
                                                            <button type="button" className="hoverable btn-cancel">
                                                                Cancel Subscription
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card-deck">
                                            <div className="card my-4">
                                                <div className="card-body">
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-between">
                                                            <div className="text-left">
                                                                <small className="field-name">Device ID</small>
                                                                <div className="device-id"></div>
                                                            </div>
                                                            <button type="button" className="btn btn-custom btn-in-dashboard3 btn-lg shadow-none">
                                                                Reset Machine
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div className="card my-4">
                                                <div className="card-body">
                                                    <div className="container-fluid overflow-hidden">
                                                        <div className="row justify-content-between">
                                                            <div className="text-left">
                                                                <small className="field-name">Tools</small>
                                                                <div className="device-id"></div>
                                                            </div>
                                                            <button type="button" className="btn btn-custom btn-in-dashboard3 btn-lg shadow-none">
                                                                Access Tools
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                        
                    <ul className="circles">
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>

                    <div className="footer footer-light bg-transparent justify-content-between">
                        <button onClick={handleLogOut} className="btn btn-custom btn-lg shadow-none btn-logout">
                            Logout
                        </button >
                    </div>
                    {/* <UpdateCardModal modalIsOpen={cardModalIsOpen} setModalIsOpen={setCardModalIsOpen} />
                    <UpdateEmailModal modalIsOpen={emailModalIsOpen} setModalIsOpen={setEmailModalIsOpen} />
                    <UnbindLicenseModal modalIsOpen={unbindModalIsOpen} setModalIsOpen={setUnbindModalIsOpen} /> */}
                </div>
            </div>
        </div>
    )
}

export default Dashboard;