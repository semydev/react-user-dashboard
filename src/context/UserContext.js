import React, { useReducer, createContext } from 'react';

export const UserContext = createContext(null);

export const SET_USER = 'SET_USER';

export const initialState = {
  binded: false,
  email: '',
  isLoggedIn: false,
  subscription: {
    billing_email: '',
	  billing_name: '',
    subscribed_since: '',
    expiration: '',
    card_details: {
        card_id: '',
        card_type: '',
        exp_month: '',
        exp_year: '',
        last_4: ''
    },
    currency: 'AUD',
    price: '',
    active_subscription: "Not Active",
  }
};

const reducer = (state, action) => {
  if (action.type === SET_USER) {
    return { ...action.payload.value };
  }
  return state;
};

export const UserContextProvider = (props) => {
  const { passedInValue, children } = props;
  const [state, dispatch] = useReducer(reducer, passedInValue || initialState);
  return <UserContext.Provider value={[state, dispatch]}>{children}</UserContext.Provider>;
};
